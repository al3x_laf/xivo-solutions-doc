########################
Configuration Management
########################

.. _callbacks:

=========
Callbacks
=========

The callback system in XivoCC aims at performing outgoing calls to specific numbers,
to which some information can be associated such as a description ar a personal name.

The core object of the callback system is the **callback request**. A callback request is made of the following fields:

- First name of person to call
- Last name
- Phone number
- Mobile phone number
- Company name
- Description
- Due date

Each callback request is associated to a predefined **callback period**, which represents the preferred interval of the day
in which the call should be performed.

A callback request cannot exist on its own: it must be stored in a **callback list**, which is itself associated to a queue.

Once a callback request has been performed, it generates a **callback ticket**. This ticket sums up the original information of the callback request,
but adding some new fields:

- Start date: date at which the callback request was actually performed
- Last update: date of the last modification of the ticket
- Comment
- Status : the result of the callback
- Agent: the Call Center agent who performed the callback

.. _callback_lists:

Callback Lists
##############

A callback list is an object which will contain callback request. It is associated to a queue,
and several callback lists can be associated to the same queue.

.. figure:: callback_lists.png
    :scale: 80%

Once created, a list can be populated whether through the :ref:`Callbacks tab <ccmanager_callbacks>` of the CCManager, or programmatically
through the web services of the configuration server.

.. _callback_periods:

Callback Periods
################

A callback period represents an interval of the day, bounded by a start date and an end date. It can be set as the default interval, so that
a newly created callback request will be associated to this period if none is specified.

.. figure:: callback_periods.png
    :scale: 80%
