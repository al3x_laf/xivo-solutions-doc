.. _web-desktop-assistant:

***********************
Web / Desktop Assistant
***********************

**What is the XiVO Web Assistant ?**

The *XiVO Web Assistant* is a Web application that enables a user to:

* search contacts and show their presence, phone status
* make calls through physical phone or using WebRTC
* access voicemail
* enable call forwarding
* show history of calls

**What is the XiVO Desktop Assistant ?**

The *XiVO Desktop Assistant* is the installable version of the  `Web Assistant` application. It offers the same features as `web-assistant`, plus

* show integrated popup when receiving call
* get keyboard shortcut to answer/hangup and make call using **Select2Call** feature
* handle `callto:` and `tel:` links. The assistant will automatically dial the number when you click this type of link.

.. note:: All features available in `Ẁeb Assistant` are existing in `Desktop Assistant`.

.. _web-assistant:

=============
Web Assistant
=============

Login
=====

To login, you must have a user configured on the *XiVO PBX* with:

* XiVO Client enabled,
* Login, password and profile configured
* A configured line with a number

.. warning:: If a user tries to login without a line, an error message is displayed and
             user is redirected to the login page (this applies also to :ref:`desktop-assistant` )

.. figure:: errornoline.png
   :scale: 80%

.. note:: A **Remember me** option is available at prompt page to keep you signed in, when you want automatic login.

Search
======

You can use the search section to lookup for people in the company:

.. figure:: search.png
   :scale: 100%

To enable this feature, you must configure the directories in the *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.


.. note:: Integration note: the *Web Assistant* support only the display of

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email


Forwarding Calls
================

From web assistant you can forward call to any another number just by clicking on action button as seen on following screenshot:

.. figure:: action.png
   :scale: 100%

Once you entered a number, you just need to **confirm** or **dismiss** to either cancel or revert the existing forward.

.. figure:: forward.png
   :scale: 100%


You know that your calls are forwarded once you see the following logo in the header bar :

.. figure:: forward_logo.png
   :scale: 100%

.. note:: If calls are redirected, the forward number will be displayed under your name.

Favorites
=========

Click on the star to put a contact in its list of favorites.
Favorites must be configured in the *XiVO PBX* as described in :ref:`dird-favorites-configuration`.

Phone integration
=================

The *Web Assistant* can integrate with the phone to :

* Call / Hangup
* Put on hold,
* Do direct or indirect transfers
* Make a conference

As these features are closely linked to the phone to work, you must check :ref:`phone_integration_support` and follow the :ref:`phone_integration_installation` page.


Once, you're phone is properly configured and you are connected as a user, you know that your using SIP phone once you see the following logo in the header bar :

.. figure:: fixed_logo.png
   :scale: 100%


WebRTC integration
==================

The *Web Assistant* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.

You know that your using WebRTC once you see the following logo in the header bar :

.. figure:: webrtc_logo.png
   :scale: 100%

`*55` (echo test)
-----------------

To test your microphone and speaker, you may call the echo test application. After a welcome message, the application will
echo everything what you speak.

 1. Dial the `*55` number from your *Desktop Assistant*.
 2. You should hear the "Echo" announcement.
 3. After the announcement, you will hear back everything you say.
     If you hear what you are saying it means that your microphone and speakers are working.
 4. Press `#` or hangup to exit the echo test application.



.. _desktop-assistant:

=================
Desktop Assistant
=================

The *XiVO Desktop Assistant* is the installable version of the  :ref:`web-assistant`. It is available for Windows (64bits) and Linux
Debian based distributions (64bits).


Navigation
==========

On first launch the application will display the settings page and ask for the `XiVO xucmgt` application address.

.. figure:: navigation.png
   :scale: 100%

The top menu allows you to navigate either to the application or to the settings page. If you did not enter any setting, the application will redirect you to the settings page.


About
=====

By clicking the **?** menu you will open a popup that show you technical information about the application that can be used to report bugs.


Settings
========

.. figure:: settings.png
   :scale: 100%

Application Options
-------------------

* **Launch at startup** if enabled, the app starts automatically when you log in to your machine.
* **Close in tray** if enabled, the app stays running in the notification area after app window is closed.


Global keyboard shortcut and Select 2 Call
------------------------------------------


This field allow you to define one shortcut in application to handle all basic actions that can be done for managing calls. With one combination keypress you should be able to:

* **Call** the phone number contained in your clipboard (a.k.a **Select2Call**)
* **Answer** a call if phone is ringing
* **Hangup** if you are already on call

.. note::
  | To be able to **call** someone, you **must** beforehand have copied in your clipboard a phone number from any source (web page, e-mail, text document...)

   * **Linux**: select phone number then trigger `shortcut`
   * **Windows**:  select phone number, type ``Ctrl+C`` then trigger `shortcut`

  Default **Select2Call** shortcut is ``Ctrl+Space`` for **Windows** and **Linux**, you can either change it or disable it by leaving the field blank.

.. warning:: You must be logged in for using global shortcut and automatic dialing to work.


Handling callto: and tel: URLs
------------------------------

The *Desktop Assistant* can handle telephone number links that appear in web pages. The *Desktop Assistant* will
automatically dial the number when you click on a link.

It is supported on both Windows and Linux Debian based distributions (with a desktop environment compatible with Freedesktop_).

Note that this feature may need some manual configuration steps. See `Known limitations`_ paragraph.

.. _Freedesktop: https://www.freedesktop.org/wiki/


Protocol and connection URL
---------------------------

These two sections allows you to specify the protocol and address of the *xucmgt* application.

* Check **Secure** if you use *HTTPS* protocol to connect to the *xucmgt* application or check **Unsecure** otherwise.
* Enter the *xucmgt* application host address and port.


Update
======

On Windows, the application will check at startup for a new version of the application and offer to upgrade if one is available.

.. figure:: update.png
   :scale: 100%


On Debian, the update relies on the package manager behavior. However you can check for any update by issuing the following commands:

.. code-block:: bash

                sudo apt-get update
                apt-cache policy xivo-desktop-assistant


Startup options
===============

The Desktop Assistant can be started with following options:

* ``--ignore-certificate-errors`` to disable certificate verification, this option is meant **only** for test purposes. You can use it with self-signed certificates.
* ``-d`` to enable debug menu items

.. note:: On **Windows** the options must not be set to the shortcut but to the ``xivo-desktop-assistant.exe`` application located in ``C:\Users\<USER>\AppData\Local\xivo\app-<VERSION>\``


Known limitations
=================

* `callto:` or `tel:` URLs : click on links using protocol `callto:` or `tel:` on **Windows** doesn't work if any version of `Skype` / `Lync` is installed on the PC. However **Manual association** can be done as explained here : https://superuser.com/a/1118466


Troubleshoot
============

If you can't log in the desktop assistant (application said that it can't log and just give you the possibility to retry or to change parameters) and if you observe some errors about certificate in debug mode, you should

* Check that you installed correctly the certificate under `/etc/docker/nginx/ssl` (:ref:`webrtc-ssl-cert`).
* Take care if your move a `*.cer` to `*.crt`. You must concatenate a key file to your `*.cer` (``cat certifcate.cer certificate.key > certificate.crt``). Just rename it will not work
* Check that XUC_HOST in `/etc/docker/compose/custom.env` is also configured with the same FQDN as in the certificate, not the IP address.
