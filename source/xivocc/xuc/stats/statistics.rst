.. highlightlang:: rest

.. _statistics:

##########
Statistics
##########

==============
Exposed by xuc
==============
Queue statistics
-----------------

These real time statistics are calculated nearly in real time from the queue_log table
Statistic are reset to 0 at midnight (24h00) can be changed by configuration

Real time calculated Queue statistic
------------------------------------

+------------------------------------+------------------------------------------------------------+
| name                               | Description                                                |
+====================================+============================================================+
| TotalNumberCallsEntered            | Total number of calls entered in a queue                   |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAbandonned         | Total number of calls abandoned in a queue (not answered)  |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAbandonnedAfter15  | Total number of calls abandoned after 15 seconds           |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAnswered           | Total number of calls answered                             |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAnsweredBefore15   | Total number of calls answered before 15 seconds           |
+------------------------------------+------------------------------------------------------------+
| PercentageAnsweredBefore15         | Percentage of calls answered before 15 seconds             |
|                                    | over total number of calls entered                         |
+------------------------------------+------------------------------------------------------------+
| PercentageAbandonnedAfter15        | Percentage of calls abandoned after 15 seconds             |
|                                    | over total number of calls entered                         |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsClosed             | Total number or calls received when queue is closed        |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsTimeout            | Total number or calls diverted on queue timeout            |
+------------------------------------+------------------------------------------------------------+

All queue statistics counters are also available for the sliding last hour by adding LastHour to the name
.i.e. TotalNumberCallsAbandonnedLastHour

For percentage, it is the mean of the sliding last hour value

Other queue statistics
----------------------

Other queue statistics are calculated by xivo cti server

* AvailableAgents
* TalkingAgents
* LongestWaitTime
* WaitingCalls
* EWT

Definition in xivo documentation `xivo documentation <http://documentation.xivo.fr/production/contact_center/supervision/supervision.html?highlight=ewt#queue-list>`_


Calculated Agent statistics
---------------------------

+------------------------------------+---------------------------------------------------------------------------------+
| name                               | Description                                                                     |
+====================================+=================================================================================+
| PausedTime                         | Total time agent in pause                                                       |
+------------------------------------+---------------------------------------------------------------------------------+
| WrapupTime                         | Total time agent in wraup                                                       |
+------------------------------------+---------------------------------------------------------------------------------+
| ReadyTime                          | Total time agent ready                                                          |
+------------------------------------+---------------------------------------------------------------------------------+
| InbCalls                           | Total number of inbound calls received internal and external                    |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAcdCalls                        | Total number of inbound ACD calls received internal and external                |
+------------------------------------+---------------------------------------------------------------------------------+
| InbCallTime                        | Total time for inbound calls received internal and external                     |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAcdCallTime                     | Total time for inbound ACD calls received internal and external                 |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAcdCallTime                     | Total time for inbound ACD calls received internal and external                 |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAnsCalls                        | Answered inbound calls received internal and external                           |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAnsAcdCalls                     | Answered inbound ACD calls received internal and external                       |
+------------------------------------+---------------------------------------------------------------------------------+
| InbUnansCalls                      | Unanswered inbound calls received internal and external                         |
+------------------------------------+---------------------------------------------------------------------------------+
| InbUnansAcdCalls                   | Unanswered inbound ACD calls received internal and external                     |
+------------------------------------+---------------------------------------------------------------------------------+
| InbPercUnansCalls                  | Percentage of unanswered inbound calls received internal and external           |
+------------------------------------+---------------------------------------------------------------------------------+
| InbPercUnansAcdCalls               | Percentage of unanswered inbound ACD calls received internal and external       |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAverCallTime                    | Average time for inbound calls received internal and external                   |
+------------------------------------+---------------------------------------------------------------------------------+
| InbAverAcdCallTime                 | Average time for inbound ACD calls received internal and external               |
+------------------------------------+---------------------------------------------------------------------------------+
| OutCalls                           | Total number of outbound calls received internal and external                   |
+------------------------------------+---------------------------------------------------------------------------------+
| LoginDateTime                      | Last login date time                                                            |
+------------------------------------+---------------------------------------------------------------------------------+
| LogoutDateTime                     | Last logout date time                                                           |
+------------------------------------+---------------------------------------------------------------------------------+

**Terms:**

inbound ACD calls
  all calls received by an agent via ACD.

inbound calls
  all calls received by an agent, internal, external or ACD calls.

oubound calls
  all calls dialed by an agent, internal or external calls.

Agent statistics are calculated internaly on a daily basis and reset to 0 at midnight (default configuration). see javascript api

If some status are configured in xivo cti server with activate pause to all queue = true, additionnal statistics computing the total
time in not ready with this status are calculated. This statistics name is equal to the presence name configuration in XiVO.

