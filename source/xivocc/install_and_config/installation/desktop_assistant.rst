*****************
Desktop Assistant
*****************

The desktop assistant is available through the xucmgt application so you need to deploy this container first.

Windows (64bits)
================

To download the latest version available on your environment, just open the following url from your computer::

  http://<xucmgt_host>:<xucmgt_port>/install/win64

and then start the downloaded program.

.. note::

          If you have a secured installation (using https/wss) the port can be omitted as the default
          port is already 443, generally speaking use the web-assistant URL followed by `/install/win64`.

Linux (Debian 64bits)
=====================

To install the latest version, you need to add a repository linked to the xucmgt host. Edit your /etc/apt/sources.list and add the following line:

.. code-block:: bash

   deb http://<xucmgt_host>:<xucmgt_port>/updates/debian jessie contrib

Then run

.. code-block:: bash

   sudo apt-get update
   sudo apt-get install xivo-desktop-assistant

.. note:: This repository is currently not signed at all.
