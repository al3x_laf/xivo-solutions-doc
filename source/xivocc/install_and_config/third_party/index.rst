=======================
Third Party Integration
=======================

Third party web application integration is possible inside the XucMgt Agent application since XucMgt version 1.49.0. Upon each call, you can display a custom tab inside the agent interface:

.. figure:: third_party_pane.png
    :scale: 60%

Workflow
--------

When a call is ringing on the agent phone, the Application will call the external web service (see :ref:`xucmgt_3rd_configuration` below). The web service response will dictate the behaviour of the integration. For example, if the speficied action is to open the application when the call is hung up, a new tab will be created and opened inside the agent interface, showing the content specified by the web service response. (see :ref:`xucmgt_3rd_api` for available options).

When the work is complete in the integrated application, the application must post a message to terminate the third party application pane inside the agent application (see :ref:`xucmgt_3rd_complete`).

.. figure:: third_party_workflow.png
    :scale: 60%

.. _xucmgt_3rd_configuration:

Configuration
-------------

You need to specify the third party application web service url to integrate this application inside the XucMgt Agent interface. This can be done by giving a value to the ``THIRD_PARTY_URL`` environment variable
in the :file:`/etc/docker/compose/custom.env` file ::

  ...
  THIRD_PARTY_URL=http://some.url.com/ws/endpoint

The speficied URL must be accessible from the client browser (i.e. the end user of the Agent application). The call wil be made from his browser.

.. _xucmgt_3rd_api:

Web Service API
---------------

The Web Service url specified in the ::ref:`xucmgt_3rd_configuration` must conforms to the following behaviour.

The service will receive a POST request with a payload as ``application/json``, for example::

    {
	"user":{
	    "userId":4,
	    "agentId":1,
	    "firstName":"James",
	    "lastName":"Bond",
	    "fullName":"James Bond"
	},
	"callee":"1000",
	"caller":"1001",
	"queue":{
	    "id":2,
	    "name":"trucks",
	    "displayName":"Trucks",
	    "number":"3001"
	},
	"userData":{
	    "XIVO_CONTEXT":"default",
	    "XIVO_USERID":"2",
	    "XIVO_SRCNUM":"1001",
	    "XIVO_DSTNUM":"3001"
	}
    }

- ``user`` contains the connected user information
- ``callee`` contains the number called
- ``queue`` queue properties
- ``userData`` call data presented by Xivo  

The Web service must answer with an ``application/json`` content. For example::
  
    {
	"action":"open",
	"event":"EventReleased",
	"url":"/thirdparty/open/6bd37819-b4a6-43d3-8fa3-6eb6489bb705",
	"autopause":true,
	"title":"Third Party Sample"
    }

or::
  
    {
	"action":"none"
    }

- ``action`` is one of ``"open"`` or ``"none"``
- ``event`` is one of ``"EventRinging"``, ``"EventEstablished"``, ``"EventReleased"``. The third party application will be opened when one the specified event occurs
- ``url`` should be the url to open inside the application. This url should point to a valid web application that can be specific for each call.
- ``autopause`` if set to true, the agent will be put on pause when the application pane is opened and back to ready when the application is completed.
- ``title`` will set the title of the tabs that will be opened.

**Warning**, when the XucMgt application and the integrated application are on different server, domain, url,... (which should be common case), You may get CORS_ errors. To workaround this issue, you should implement the OPTIONS request on your web service. This method will be called by the browser before issuing the POST request to ensure the target web server allows calls from the original application. You application must set at least the following headers in order to overcome the CORS_ errors:

- ``Access-Control-Allow-Origin``: * or the domain hosting the XucMgt application
- ``Access-Control-Allow-Methods``: POST, OPTIONS (at least)
- ``Access-Control-Allow-Headers``: Origin, X-Requested-With, Content-Type, Accept (at least)

.. _CORS: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing

.. _xucmgt_3rd_complete:

Completion
----------

Once the work is complete inside the third party application, it should post a completion message (``closeThirdParty``) to the application using the `Web Messaging API`_.

For example, here is how to define a close method in javascript to send the message to the hosting application and bind it to a simple button::
  
  (function () {
	function close() {
            parent.window.postMessage("closeThirdParty", "*");
	}

	document.getElementById("close").addEventListener("click", close, false);
  })();

.. _Web Messaging API: https://en.wikipedia.org/wiki/Web_Messaging
