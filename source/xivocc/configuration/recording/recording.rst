.. _recording_configuration:

***********************
Recording configuration
***********************

This page describes how to configure the recording feature for XiVO Solutions.
This feature needs some additionnal :ref:`installation steps which are described here <recording_xpbx>`.


Configure recording
===================

To configure recording there are two steps to follow on *XiVO PBX*:

#. Add link towards Recording Server,
#. and Enable recording with the subroutines.


1. Add link towards Recording Server
------------------------------------

.. note:: Steps to be done on **XiVO PBX**

The first step is to configure the link towards the Recording Server by running the configuration script:

.. code-block:: bash

	xivocc-recording-config

During the configuration, you will be asked for :

* the Recording Server IP (i.e. 192.168.0.2)
* the *XiVO PBX* name (it must not contain any space or "-" character).
  If you configure more more than one *XiVO PBX* on the same Recording Server, you must give a different name to each of them.

After having configured the recording, you have to enable it via sub-routines. See below.


2. Enable recording with subroutines
------------------------------------

.. note:: Steps to be done on **XiVO PBX**

To enable the recording you have to configure one of the shipped subroutines.

The package ``xivocc-recording`` (see :ref:`recording installation section <recording_xpbx>`) ships the following dialplan subroutines :

===================================  ==========================
Subroutine                           Description
===================================  ==========================
``xivocc-incall-recording``          Records incoming calls
``xivocc-incall-recording-paused``   Records incoming calls, but record starts in paused state and can be activated by the agent (see :ref:`agent recording configuration <agent_recording>`)
``xivocc-outcall-recording``         Records outgoing calls
``xivocc-outcall-recording-paused``  Records outgoing calls, but record starts in paused state and can be activated by the agent (see :ref:`agent recording configuration <agent_recording>`)
===================================  ==========================

These subroutines are to be configured on the following *XiVO PBX* objects (either globally or per-object):

.. warning:: They **MUST** be configured **only** on the following objects. Other configuration **are not supported**.

* Incalls,
* and/or Queues,
* and/or Users,
* and/or Outcall

----

.. note:: Here is an **example** if you want to enable recording for:

    * *all* Outcalls but started in pause state,
    * *all* Queues,
    * and *only on* Incall 0123456789

    Then you would have to:

    #. Enable call recording for *all* queues and outcalls by editing
       the configuration file :file:`/etc/xivo/asterisk/xivo_globals.conf` and set::

        XIVO_PRESUBR_GLOBAL_QUEUE = xivocc-incall-recording
        ...
        XIVO_PRESUBR_GLOBAL_OUTCALL = xivocc-outcall-recording-paused

    #. Enable the call recording for incall 0123456789 by editing it via the *XiVO PBX* web interface
       and set the field :menuselection:`Pre-process subroutine` to ``xivocc-incall-recording``


.. _recording_filtering_configuration:

Recording filtering configuration
=================================

.. note:: Steps to be done on **XiVO CC**

After having followed above paragraphs, you can also configure the recording filtering.

#. Add a user with Administrateur rights for Recording Server:
   
   #. Connect to the Config Management interface : http://<XIVO_CC_IP>:9100 (login avencall/superpass),
   #. Add one of the *XiVO PBX* user giving him *Administrateur* rights,

#. Configure excluded numbers on Recording Server
   
   #. Then, connect with this user to the Recording Server interface : http://<XIVO_CC_IP>:9400
   #. Navigate to the page ``Contrôle d'enregistrement`` and add the numbers to be excluded from the recording.


In list :menuselection:`Destinataire de l'appel (Numéro entrant, File d'attente, Utilisateur)` declare the:

* XiVO Incalls numbers,
* XiVO Queues numbers,
* or XiVO Users numbers

to be excluded from the recording on incoming or internal call. These numbers will be checked by the ``xivocc-incall-recording`` subroutines.

.. note:: numbers must be entered as they first appear in dialplan (check is made against XIVO_DSTNUM dialplan variable).

In list :menuselection:`Emetteur ou destinataire d'un appel sortant (Utilisateur ou numéro appelé externe)` declare the:

* XiVO Users internal numbers

to be excluded from recording on outgoing calls. These numbers will be checked by the ``xivocc-outcall-recording`` subroutines.

.. note:: check is made against XIVO_SRCNUM dialplan variable.


.. _recording_gw_configuration:

Gateway recording configuration
===============================

*No doc currently*
