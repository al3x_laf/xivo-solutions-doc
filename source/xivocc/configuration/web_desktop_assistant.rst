.. _webassistant_configuration:

***********************
Web / Desktop Assistant
***********************

.. _webassistant_disable_webrtc:

Disabling WebRTC
================

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.
