XiVO Stats
==========

XiVO db replication does not replicate call data
-------------------------------------------

After experiencing a 'no space left on device' and restarting containers, it can sometimes happen that the data from XiVO is not replicated anymore. Container *xivocc_replic_1* logs show the following error::

   xivo_replic_1       | liquibase.exception.LockException: Could not acquire change log lock.  Currently locked by fe80:0:0:0:42:acff:fe11:8%eth0 (fe80:0:0:0:42:acff:fe11:8%eth0) since 4/10/17 3:28 PM


Error is caused by a lock in liquibase db. Follow the below steps in order to fix the issue:

#. Find that a lock is active (even with xivo_stats, xivo_replic, pack_reporting containers stopped)::

    xivo_stats=# select * from databasechangeloglock;
    id | locked |       lockgranted       |                            lockedby                             
    ----+--------+-------------------------+-----------------------------------------------------------------
    1 | t      | 2017-04-10 15:28:10.684 | fe80:0:0:0:42:acff:fe11:8%eth0 (fe80:0:0:0:42:acff:fe11:8%eth0)

#. Delete the lock::

    xivo_stats=# truncate databasechangeloglock;
    xivo_stats=# select * from databasechangeloglock;
    id | locked | lockgranted | lockedby 
    ----+--------+-------------+----------

#. Restart the containers::

    xivocc-dcomp start pack_reporting
    xivocc-dcomp start xivo_replic
    xivocc-dcomp start xivo_stats

Now, if xivo_stats is stuck in exit (126), try a *docker rm -v {xivo_stats contener id}* followed by a *xivocc-dcomp up -d*.
