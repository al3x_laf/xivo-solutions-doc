******
WebRTC
******

General notes
=============

.. note:: added in version 2016.04

XiVO comes with a WebRTC lines support, you can use in with XiVO *Web Assistant* and *Desktop Assistant*.
Before starting, please check the :ref:`webrtc_requirements`.

Current WebRTC implementation requires following configuration steps:

* configure :ref:`asterisk to accept websocket connections <configure_xpbx_webrtc>`,
* and create user :ref:`with one line configured for WebRTC <configure_user_with_webrtc_line>`. To have user with both SIP and WebRTC line is not supported.


.. _configure_xpbx_webrtc:

Configuration of XiVO PBX for WebRTC
====================================

.. warning:: Security warning: when enabling WebRTC you need to ensure that you do it securely:

    #. by securing the access to the ARI,
    #. and by securing (e.g. via an external firewall) the access to the asterisk HTTP server (which listens on port 5039).

**First**, secure ARI connection (this step is **very important** otherwise your *XiVO PBX* won't be secure) :

* Generate a password (e.g. with ``pwgen -s 16``)
* Edit file :file:`/etc/asterisk/ari.conf`
* replace::

   password = Nasheow8Eag

  by::

   password = <YOUR_GENERATED_PASSWORD>

* Then, update password in xivo-ctid-ng configuration,
* Create a file :file:`/etc/xivo-ctid-ng/conf.d/01-xivocc-webrtc.yml`::

   touch /etc/xivo-ctid-ng/conf.d/01-xivocc-webrtc.yml

* With the following content::

   # Custom password
   ari:
     connection:
         password: <YOUR_GENERATED_PASSWORD>


Then, open asterisk HTTP server to accept outside websocket connections:

* Edit file :file:`/etc/asterisk/http.conf`
* replace::

   bindaddr=127.0.0.1

 by::

   bindaddr=0.0.0.0

* Restart XiVO PBX services::

   xivo-service restart


.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Edit the line created and, in the *Advanced* tab, add `webrtc=yes` options:

.. figure:: webrtc_line.png
    :scale: 100%


Manual configuration of user with WebRTC line
==============================================

For the records 

.. toctree::
    
    webrtc_manualconf

