.. _switch-to-xivo.solutions:

************************
Switch to xivo.solutions
************************

.. note:: If you are in *XiVO PBX* **below 2016.03** you should first switch to xivo.solutions mirrors.

In order to do that follow the following procedure:

#. Download the ``switch-to-xivo-solutions`` script::

    wget http://mirror.xivo.solutions/debian/tools/migration-tools/switch-to-xivo-solutions.sh
    chmod +x ./switch-to-xivo-solutions.sh

#. Execute the script::

    ./switch-to-xivo-solutions.sh

    ...

    Your XiVO has been switched to xivo.solutions successfully.
    Votre XiVO a été basculé vers xivo.solutions avec succès.

#. Update the sources list::

    apt-get update


